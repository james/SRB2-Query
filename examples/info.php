<html>
<body>
<?php
/*
This example generates a HTML document that lists all server info, player info
and file info.
 */

/*
Copyright 2019, James R.
All rights reserved.

Redistribution and use in source form, with or without modification, are
permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of this project.
 */

include 'srb2query.php';

$query = new SRB2Query;
$query->Ask("localhost", 5029);
$info = $query->Info($addr);
if ($info)
{
	printf(
		'<h1 id="version">' .
		"\n" .
		'%s %d.%d.%d' .
		"\n" .
		'</h1>' .
		"\n",
		$info['version']['name'],
		$info['version']['major'],
		$info['version']['minor'],
		$info['version']['patch']);
	printf(
		'<h1 id="servername">' .
		"\n"    .
		'%s'    .
		"\n"    .
		'</h1>' .
		"\n",
		$query->Colorize($info['servername']));
	printf(
		'<p id="address">' .
		"\n"   .
		'%s'   .
		"\n"   .
		'</p>' .
		"\n",
		$addr);
	printf(
		'<h2>' .
		"\n" .
		'<span id="maptitle">' .
		"\n" .
		'%s' .
		"\n" .
		'</span>' .
		"\n" .
		' - ' .
		"\n" .
		'<span id="gametype">' .
		"\n" .
		'%s' .
		"\n" .
		'</span>' .
		"\n",
		$info['level']['title'],
		$info['gametype']);
	if (isset($info['kartspeed']))
		printf(
			' - ' .
			"\n" .
			'<span id="kartspeed">' .
			"\n" .
			'%s' .
			"\n" .
			'</span>' .
			"\n",
			$info['kartspeed']);
	printf(
		'</h2>' .
		"\n");
	printf(
		'<p>' .
		"\n");
	if ($info['dedicated'])
		printf(
			'<span id="dedicated">' .
			"\n" .
			'(dedicated)' .
			"\n" .
			'</span>' .
			"\n",
			$info['dedicated']);
	if ($info['mods'])
		printf(
			'<span id="mods">' .
			"\n" .
			'(mods)' .
			"\n" .
			'</span>' .
			"\n",
			$info['mods']);
	if ($info['cheats'])
		printf(
			'<span id="cheats">' .
			"\n" .
			'(cheats)' .
			"\n" .
			'</span>' .
			"\n",
			$info['cheats']);
	printf(
		'<p id="playercount">' .
		"\n" .
		'<span id="players">' .
		"\n" .
		'%d' .
		"\n" .
		'</span>' .
		"\n" .
		' of ' .
		"\n" .
		'<span id="maxplayers">' .
		"\n" .
		'%d' .
		"\n" .
		'</span>' .
		"\n" .
		' players' .
		"\n" .
		'</p>' .
		"\n",
		$info['players']['count'],
		$info['players']['max']);
	printf(
		'<p id="timeinlevel">' .
		"\n" .
		'<span id="levelseconds">' .
		"\n" .
		'%d seconds' .
		"\n" .
		'</span>' .
		"\n" .
		'</p>' .
		"\n",
		$info['level']['seconds']);

	$files = $query->Fileinfo();

	printf(
		'<hr>' .
		"\n" .
		'<h3 id="filecount">' .
		"\n" .
		'%d files' .
		"\n" .
		'</h3>' .
		"\n",
		count($files));

	printf(
		'<table id="files">' .
		"\n" .
		'<tr id="fileheader">' .
		"\n" .
		'<th id="fileheader-name">' .
		"\n" .
		'Name' .
		"\n" .
		'</th>' .
		"\n" .
		'<th id="fileheader-md5sum">' .
		"\n" .
		'MD5 Checksum' .
		"\n" .
		'</th>' .
		"\n" .
		'<th id="filehader-downloadable">' .
		"\n" .
		'Downloadable' .
		"\n" .
		'</tr>' .
		"\n");

	foreach ($files as $file)
	{
		printf(
			'<tr class="file">' .
			"\n" .
			'<td class="file-name">' .
			"\n" .
			'%s' .
			"\n" .
			'</td>' .
			"\n" .
			'<td class="file-md5sum">' .
			"\n" .
			'%s' .
			"\n" .
			'</td>' .
			"\n" .
			'<td class="file-downloadable">' .
			"\n",
			$file['name'],
			$file['md5sum']);
		if ($file['download'])
			printf(
				'Yes' .
				"\n");
		printf(
			'</td>' .
			"\n" .
			'</tr>' .
			"\n");
	}

	printf(
		'</table>' .
		"\n");

	printf(
		'<hr>' .
		"\n" .
		'<table id="playertable">' .
		"\n" .
		'<tr id="playerheader">' .
		"\n" .
		'<th id="playerheader-team">' .
		"\n" .
		'Team' .
		"\n" .
		'</th>' .
		"\n" .
		'<th id="playerheader-name">' .
		"\n" .
		'Name' .
		"\n" .
		'</th>' .
		"\n");
	if ($info['version']['name'] == 'SRB2Kart')
	{
		printf(
			'<th id="playerheader-rank">' .
			"\n" .
			'Rank' .
			"\n" .
			'</th>' .
			"\n");
		$scoreorrank = 'rank';
	}
	else
	{
		printf(
			'<th id="playerheader-score">' .
			"\n" .
			'Score' .
			"\n" .
			'</th>' .
			"\n");
		$scoreorrank = 'score';
	}
	printf(
		'<th id="playerheader-time">' .
		"\n" .
		'Play Time' .
		"\n" .
		'</th>' .
		"\n" .
		'</tr>' .
		"\n");

	foreach ($info['players']['list'] as $player)
	{
		printf(
			'<tr class="player">' .
			"\n" .
			'<td class="player-team">' .
			"\n" .
			'%s' .
			"\n" .
			'</td>' .
			"\n" .
			'<td class="player-name">' .
			"\n" .
			'%s' .
			"\n" .
			'</td>' .
			"\n" .
			'<td class="player-' . $scoreorrank . '">' .
			"\n" .
			'%s' .
			"\n" .
			'</td>' .
			"\n" .
			'<td class="player-time">' .
			"\n" .
			'<span class="player-time-seconds">' .
			"\n" .
			'%d' .
			"\n" .
			'</span>' .
			"\n" .
			'</td>' .
			"\n" .
			'</tr>' .
			"\n",
			$player['team'],
			$player['name'],
			$player[$scoreorrank],
			$player['seconds']);
	}

	printf(
			'</table>' .
			"\n");
}
?>
</body>
</html>
